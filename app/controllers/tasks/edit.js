import Controller from "@ember/controller";
import { action } from "@ember/object";

export default class TasksEditController extends Controller {
  get task() {
    const task = this.store.peekRecord("task", this.model.id);
    return task ? task : this.transitionToRoute("tasks");
  }

  @action update(e) {
    e.preventDefault();
    const input = e.target.text;
    const task = this.store.peekRecord("task", this.model.id);
    if (task) {
      task.text = input.value;
      this.transitionToRoute("tasks");
    }
  }
}
