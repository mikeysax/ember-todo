import Controller from "@ember/controller";
import { action } from "@ember/object";

export default class TasksIndexController extends Controller {
  get tasks() {
    return this.store.peekAll("task");
  }

  @action create(e) {
    e.preventDefault();
    const input = e.target.task;
    this.store.createRecord("task", {
      id: new Date().valueOf(),
      text: input.value,
    });
    input.value = "";
  }

  @action delete(id, e) {
    let task = this.store.peekRecord("task", id);
    task.destroyRecord();
  }
}
